package com.itri.toolkit.tag.bluetooth;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.util.Log;

import com.itri.toolkit.tag.object.SensorNode;
import com.itri.toolkit.tag.object.SensorNodeRecord;
import com.itri.toolkit.tag.utility.ConstUtil;
import com.itri.toolkit.tag.utility.NodeControllerUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Implement SensorTag Polling command
 */
public class NodePollingController extends NodeControllerBase {
    private static final String TAG_PREFIX = "[NodePollingController] ";
    private int checkCount = 0; //Check if the count of result is matching the count of polling nodes
    private boolean isRepeat = true; //Declare if it is repeat mode for polling request
    private boolean needScan = false; //Declare if the polling operation needed to start with scan method
    private Timer pollingTimer;
    private PollingTimerTask pollingTimerTask;
    private ArrayList<SensorNode> pollingNodeList = new ArrayList<SensorNode>(); //Target polling nodes

    public boolean isRunning = false; //Check if the polling task is processing.
    public HashMap<String, SensorNodeRecord> pollingResult = new HashMap<String, SensorNodeRecord>(); //<TagID, record>

    class PollingTimerTask extends TimerTask {
        @Override
        public void run() {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "polling timer task run : " + isRunning + ", " + pollingNodeList.toString());
            doPollingTask();
        }
    }

    public NodePollingController(Context context) {
        super(context);
    }

    public void startPollingNodes(ArrayList<SensorNode> pollingNodes, boolean repeat) {
        startPollingNodes(pollingNodes, repeat, true);
    }

    public void startPollingNodes(ArrayList<SensorNode> pollingNodes, boolean repeat, boolean needScan) {
        this.needScan = needScan;
        this.pollingNodeList.clear();
        this.pollingNodeList.addAll(pollingNodes);
        checkCount = pollingNodes.size();

        //Cancel the polling
        resetTimer();

        //If it is repeat mode, and schedule polling timer, or just do polling task once.
        isRepeat = repeat;
        if (repeat) {
            pollingTimerTask = new PollingTimerTask();
            pollingTimer = new Timer();
            pollingTimer.schedule(pollingTimerTask, NodeControllerUtil.INTERVAL_APP_POLLING * 1000);
        } else {
            doPollingTask();
        }
    }

    public HashMap<String, SensorNodeRecord> getResult() {
        return pollingResult;
    }


    public void stopPolling() {
        resetTimer();
        isRunning = false;
    }

    private NodeControllerListener nodePollingControllerListener = new NodeControllerListener() {
        @Override
        public void onAccessCharacteristic(BluetoothGatt gatt) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onAccessCharacteristic");
            readCharacteristic(NodeControllerUtil.UUID_SERVICE, NodeControllerUtil.UUID_CHARACTERISTIC_DATA);
        }

        @Override
        public void onNodeCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

        }

        @Override
        public void onNodeCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onTargetNodeQueryFinished");
            parseSensorData(characteristic);
            finishCurrentConnection();
        }

        @Override
        public void onNodeCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

        }

        @Override
        public void onAllNodeQueryFinished() {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onAllNodeQueryFinished");
            isRunning = false;
            nodeStatusListener.onAllNodeFinished(checkCount == pollingResult.keySet().size());

            for (SensorNode node : pollingNodeList) {
                node.connectionStatus = SensorNode.ConnectionStatus.POLLING_WAITING;
            }

            //If it is repeat mode, reset timer and schedule next polling timer.
            if (isRepeat) {
                resetTimer();
                pollingTimerTask = new PollingTimerTask();
                pollingTimer = new Timer();
                pollingTimer.schedule(pollingTimerTask, NodeControllerUtil.INTERVAL_APP_POLLING * 1000);
            }
        }

        @Override
        public void onFailed(String errorMsg) {
            nodeStatusListener.onNodeFailed(errorMsg);
        }

        @Override
        public void onStartToConnect(String nodeID) {
            nodeStatusListener.onNodeStart(nodeID);
        }
    };

    private void parseSensorData(BluetoothGattCharacteristic characteristic) {
        //parsing data
        byte[] byte_data = characteristic.getValue();
        int[] int_data = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 0; i < byte_data.length - 1; i++) {
            int_data[i] = NodeControllerUtil.convertByteToInt(byte_data[i]);
        }
        int_data[11] = byte_data[11];

        String time = "20" + int_data[0]
                + "-" + ((int_data[1] > 9) ? int_data[1] : ("0" + int_data[1]))
                + "-" + ((int_data[2] > 9) ? int_data[2] : ("0" + int_data[2]))
                + "T" + ((int_data[4] > 9) ? int_data[4] : ("0" + int_data[4]))
                + ":" + ((int_data[5] > 9) ? int_data[5] : ("0" + int_data[5]))
                + ":" + ((int_data[6] > 9) ? int_data[6] : ("0" + int_data[6]));


        String temperature;
        if (int_data[7] == 0) {
            temperature = "" + ((int_data[8] > 0) ? int_data[8] : "") + int_data[9] + "." + int_data[10];
        } else {
            temperature = "-" + int_data[8] + int_data[9] + "." + int_data[10];
        }

        String battery = String.valueOf(int_data[11]);

        Log.v(ConstUtil.TAG, TAG_PREFIX + "parse data : time - " + time + ", temp - " + temperature + ", battery : " + battery);
        //Put data to polling result.
        pollingResult.put(getCurrentTargetNodeID(), new SensorNodeRecord(getCurrentTargetNodeID(), time, temperature, battery));
    }

    private void doPollingTask() {
        setNodeControllerListener(nodePollingControllerListener);
        for (SensorNode node : pollingNodeList) {
            node.connectionStatus = SensorNode.ConnectionStatus.POLLING;
        }
        if (isRunning) {
            return;
        } else {
            isRunning = true;
            pollingResult.clear();
            //Check the polling operation needed to start with scan method
            if (needScan) {
                setTargetNodeList(pollingNodeList);
                startBLEOperation();
            } else {
                startConnectFoundedDevices(pollingNodeList);
            }
        }
        nodeStatusListener.onStart();
    }

    private void resetTimer() {
        if (pollingTimer != null) {
            pollingTimer.cancel();
            pollingTimer = null;
        }
        if (pollingTimerTask != null) {
            pollingTimerTask.cancel();
        }
    }
}
