package com.itri.toolkit.tag.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.itri.toolkit.tag.object.SensorNode;
import com.itri.toolkit.tag.utility.ConstUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Implement Bluetooth device connection
 */
public class BLEController {
    private final static String TAG_PREFIX = "[BLEController] ";
    private final static int SCAN_PERIOD = 20000;
    private final static int CONNECTION_TIMEOUT = 5000;
    private final static int NEXT_INTERVAL = 500;

    private static boolean scanRegistered = false;
    private static BLEController mInstance;

    private boolean mScanning = false;

    //For retry count variable
    private static final int RETRY_LIMIT = 100;
    private int retryWrite = 0;
    private int retryRead = 0;

    private Context mContext;
    private BluetoothAdapter mBLEAdapter;
    private BluetoothGatt mBluetoothGatt;
    private BLEManagerListener mBleManagerListener;


    private ArrayList<BluetoothDevice> mTargetDevices = new ArrayList<BluetoothDevice>(); //BluetoothDevices founded based on mTargetNodes
    private HashMap<String, SensorNode> mTargetNodes = new HashMap<String, SensorNode>(); //Target nodes user defined

    //Bluetooth connection status
    private int mConnectionState = BLEConnectionState.STATE_DISCONNECTED;

    private class BLEConnectionState {
        public static final int STATE_DISCONNECTED = 0;
        public static final int STATE_START_CONNECTING = 1;
        public static final int STATE_CONNECTING_CLOSE = 2;
        public static final int STATE_CONNECTED = 3;
    }

    //BLE Handler Message
    private static final int MSG_BLE_CONNECT_GATT = 501;
    private static final int MSG_BLE_GATT_CONNECTED = 502;
    private static final int MSG_BLE_GATT_DISCONNECTED = 503;
    private static final int MSG_BLE_SERVICE_DISCOVERED = 504;
    private static final int MSG_BLE_START_CONNECT_NEXT_DEVICE = 505;
    private static final int MSG_BLE_SCAN_TIMEOUT = 506;
    private static final int MSG_BLE_DISCOVER_SERVICES_TIMEOUT = 507;
    private static final int MSG_BLE_CONNECT_GATT_TIMEOUT = 508;
    private static final int MSG_BLE_DISCONNECT_GATT_TIMEOUT = 509;


    //TODO:listener/handler
    public interface BLEManagerListener {
        void onConnected();

        void onFinished(); //nodelist finished

        void onServiceDiscovered(BluetoothGatt bluetoothGatt, int status);

        void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);

        void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);

        void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic);

        void onFailed(String errorMsg);

        void onStartToConnect(String nodeID);
    }

    public BLEController(Context context) {
        this.mContext = context;
        initComponent();
    }

    public static BLEController getInstance(Context context) {
        // Return the instance
        if (mInstance == null) {
            mInstance = new BLEController(context);
        }
        return mInstance;
    }

    private void initComponent() {
        mBLEAdapter = null;
        final BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBLEAdapter = bluetoothManager.getAdapter();
    }

    public void setServiceListener(BLEManagerListener listener) {
        mBleManagerListener = listener;
    }

    public void enableBLE() {
        if (!mBLEAdapter.isEnabled()) {
            mBLEAdapter.enable();
            Log.v(ConstUtil.TAG, TAG_PREFIX + "enable BLE : " + mBLEAdapter.isEnabled());
        }
    }

    public void startOperation(ArrayList<SensorNode> nodes) {
        mTargetNodes.clear();
        for (SensorNode node : nodes) {
            mTargetNodes.put(node.ID, node);
        }
        mTargetDevices.clear();
        initComponent();
        scanDevices(true);
    }

    public void startConnectFoundedDevices() {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "startConnectFoundedDevices");
        mTargetDevices.clear();
        for (SensorNode node : mTargetNodes.values()) {
            BluetoothDevice device = mBLEAdapter.getRemoteDevice(node.mMACAddress);
            if (device != null) {
                mTargetDevices.add(device);
            }
        }
        if (mTargetDevices.size() > 0) {
            mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_CONNECT_GATT);
        }
    }

    public void startConnectRemoteDevices(ArrayList<SensorNode> nodes) {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "startConnectFoundedDevices, mBluetoothGatt : " + (mBluetoothGatt == null));
        mTargetDevices.clear();
        mTargetNodes.clear();
        HashMap<String, SensorNode> nodeMap = new HashMap<String, SensorNode>();
        for (SensorNode node : nodes) {
            nodeMap.put(node.ID, node);
        }
        mTargetNodes.putAll(nodeMap);
        for (SensorNode node : mTargetNodes.values()) {
            if (node.mMACAddress.length() > 0) {
                BluetoothDevice device = mBLEAdapter.getRemoteDevice(node.mMACAddress);
                if (device != null) {
                    mTargetDevices.add(device);
                } else {
                    Log.v(ConstUtil.TAG, TAG_PREFIX + "startConnectFoundedDevices can't find device" + node.ID);
                }
            }
        }
        if (mTargetDevices.size() > 0) {
            mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_CONNECT_GATT);
        }
    }

    public String getTargetID() {
        if (mTargetDevices.isEmpty()) {
            return "";
        } else {
            String name = mTargetDevices.get(0).getName();
            if (name != null) {
                Log.v(ConstUtil.TAG, TAG_PREFIX + "getTargetID == " + name);
                return name;
            } else {
                for (SensorNode sensorNode : mTargetNodes.values()) {
                    if (mTargetDevices.get(0).getAddress().equals(sensorNode.mMACAddress)) {
                        Log.v(ConstUtil.TAG, TAG_PREFIX + "getTargetID == " + sensorNode.ID);
                        return sensorNode.ID;
                    }
                }
                return "";
            }
        }
    }

    private void scanDevices(final boolean enable) {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "scanLeDevice - " + enable + ", register = " + scanRegistered);
        mBLEAdapter.cancelDiscovery();
        if (enable) {
            mScanning = true;
            registerBLEScanReceiver();
            mBLEAdapterHandler.sendEmptyMessageDelayed(MSG_BLE_SCAN_TIMEOUT, SCAN_PERIOD);
            if (mBLEAdapter.startDiscovery()) {// Stops scanning after a pre-defined scan period.
            } else {
                Log.v(ConstUtil.TAG, TAG_PREFIX + "scanLeDevice - failed ");
            }
        } else {
            mScanning = false;
            unRegisterBLEScanReceiver();
        }
    }

    private void registerBLEScanReceiver() {
        if (!scanRegistered) {
            IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            mContext.registerReceiver(mBLEScanReceiver, intentFilter);
            scanRegistered = true;
        }
    }

    private void unRegisterBLEScanReceiver() {
        if (scanRegistered) {
            mContext.unregisterReceiver(mBLEScanReceiver);
            scanRegistered = false;
        }
    }

    private final BroadcastReceiver mBLEScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Assert if ACTION_FOUNDED, the BLE must be scanning
            if (BluetoothDevice.ACTION_FOUND.equals(action) && mScanning) {
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.v(ConstUtil.TAG, TAG_PREFIX + "BLE status onReceive [FOUND]" + device.getName());
                if (mTargetNodes.keySet().contains(device.getName())) {
                    Log.v(ConstUtil.TAG, TAG_PREFIX + "BLE status onReceive [FIND THE DEVICE]" + device.getName());
                    if (!mTargetDevices.contains(device)) {
                        Log.v(ConstUtil.TAG, TAG_PREFIX + "BLE status onReceive [FIND THE DEVICE] and add it: " + device.getName());
                        mTargetDevices.add(device);
                    }
                }
                //All target devices found
                if (mTargetDevices.size() == mTargetNodes.size()) {
                    mBLEAdapterHandler.removeMessages(MSG_BLE_SCAN_TIMEOUT);
                    scanDevices(false);
                    for (BluetoothDevice bleDevice : mTargetDevices) {
                        mTargetNodes.get(bleDevice.getName()).mMACAddress = bleDevice.getAddress();
                    }
                    if (mConnectionState == BLEConnectionState.STATE_DISCONNECTED) {
                        mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_CONNECT_GATT);
                    } else {
                        Log.v(ConstUtil.TAG, TAG_PREFIX + "FOUND device but incorrect connect state" + mConnectionState);
                        mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_CONNECT_GATT);
                    }
                }
            }
        }
    };

    private BluetoothGattCallback mBluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onConnectionStateChange device name : " + gatt.getDevice().getName() + ", status : " + status + ", new : " + newState);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.w(ConstUtil.TAG, TAG_PREFIX + "onConnectionStateChange STATE_CONNECTED" + mConnectionState);
                if (mConnectionState == BLEConnectionState.STATE_START_CONNECTING) {
                    mBLEAdapterHandler.removeMessages(MSG_BLE_CONNECT_GATT_TIMEOUT);
                    mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_GATT_CONNECTED);
                    if (mBleManagerListener != null) {
                        mBleManagerListener.onConnected();
                    }
                }
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.w(ConstUtil.TAG, TAG_PREFIX + "onConnectionStateChange MSG_BLE_GATT_DISCONNECTED - " + mConnectionState);
                if (mConnectionState == BLEConnectionState.STATE_START_CONNECTING || mConnectionState == BLEConnectionState.STATE_CONNECTED) {
                    if (mBleManagerListener != null) {
                        mBleManagerListener.onFailed("Can't find Sensor");
                    }
                } else if (mConnectionState == BLEConnectionState.STATE_CONNECTING_CLOSE) {
                    mBLEAdapterHandler.removeMessages(MSG_BLE_DISCONNECT_GATT_TIMEOUT);
                }
                mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_GATT_DISCONNECTED);
            } else {
                Log.w(ConstUtil.TAG, TAG_PREFIX + "onConnectionStateChange failed - " + newState);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.w(ConstUtil.TAG, TAG_PREFIX + "onServicesDiscovered status : " + status);
            mBLEAdapterHandler.removeMessages(MSG_BLE_DISCOVER_SERVICES_TIMEOUT);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.w(ConstUtil.TAG, TAG_PREFIX + "onServicesDiscovered received gatt");
                mBluetoothGatt = gatt;
                if (mBleManagerListener != null) {
                    mBleManagerListener.onServiceDiscovered(gatt, status);
                }
                mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_SERVICE_DISCOVERED);
            } else {
                if (mBleManagerListener != null) {
                    mBleManagerListener.onFailed("Services failed - " + status);
                }
                finishCurrentConnection();
            }
        }


        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.w(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicRead SUCCESS");
                if (mBleManagerListener != null) {
                    mBleManagerListener.onCharacteristicRead(gatt, characteristic, status);
                }
                retryRead = 0;
            } else {
                if (mBleManagerListener != null) {
                    mBleManagerListener.onFailed("讀取失敗 - " + status);
                }
                if (retryRead < RETRY_LIMIT) {
                    Log.w(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicRead received failed retry : " + status);
                    gatt.readCharacteristic(characteristic);
                    retryRead++;
                } else {
                    Log.w(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicRead failed : " + status);
                    finishCurrentConnection();
                    retryRead = 0;
                }
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.w(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicWrite - SUCCESS");
                if (mBleManagerListener != null) {
                    mBleManagerListener.onCharacteristicWrite(gatt, characteristic, status);
                }
                retryWrite = 0;
            } else {
                if (mBleManagerListener != null) {
                    mBleManagerListener.onFailed("寫入失敗 - " + status);
                }
                if (retryWrite < RETRY_LIMIT) {
                    Log.w(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicWrite received failed retry: " + status);
                    gatt.writeCharacteristic(characteristic);
                    retryWrite++;
                } else {
                    finishCurrentConnection();
                    retryWrite = 0;
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.w(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicChanged");
            if (mBleManagerListener != null) {
                mBleManagerListener.onCharacteristicChanged(gatt, characteristic);
            }
        }
    };

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public boolean readCharacteristic(UUID service, UUID characteristic) {
        retryRead = 0;
        String errorMsg = "";
        if (mBLEAdapter != null && mBluetoothGatt != null) {
            BluetoothGattService bluetoothGattService = mBluetoothGatt.getService(service);
            if (bluetoothGattService != null) {
                BluetoothGattCharacteristic bluetoothGattCharacteristic = bluetoothGattService.getCharacteristic(characteristic);
                if (bluetoothGattCharacteristic != null) {
                    if (mBluetoothGatt.readCharacteristic(bluetoothGattCharacteristic)) {
                        return true;
                    } else {
                        errorMsg = "readCharacteristic failed - return false";
                    }
                } else {
                    errorMsg = "readCharacteristic failed - characteristic not found!";
                }
            } else {
                errorMsg = "readCharacteristic failed - service not found!";
            }
        } else {
            errorMsg = "readCharacteristic failed - lost connection!";
        }
        if (mBleManagerListener != null) {
            mBleManagerListener.onFailed(errorMsg);
        }
        finishCurrentConnection();
        return false;
    }

    public boolean writeCharacteristic(UUID service, UUID characteristic, byte[] data) {
        retryWrite = 0;
        String errorMsg = "";
        if (mBLEAdapter != null && mBluetoothGatt != null) {
            BluetoothGattService bluetoothGattService = mBluetoothGatt.getService(service);
            if (bluetoothGattService != null) {
                BluetoothGattCharacteristic bluetoothGattCharacteristic = bluetoothGattService.getCharacteristic(characteristic);
                if (bluetoothGattCharacteristic != null) {
                    bluetoothGattCharacteristic.setValue(data);
                    if (mBluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)) {
                        return true;
                    } else {
                        errorMsg = "readCharacteristic failed - return false";
                    }
                } else {
                    errorMsg = "readCharacteristic failed - characteristic not found!";
                }
            } else {
                errorMsg = "readCharacteristic failed - service not found!";
            }
        } else {
            errorMsg = "readCharacteristic failed - lost connection!";
        }
        if (mBleManagerListener != null) {
            mBleManagerListener.onFailed(errorMsg);
        }
        finishCurrentConnection();
        return false;
    }

    //BLE error state, timeout, need to skip current connection
    //External Caller : Read/Write operation finished, need to start next one
    public void finishCurrentConnection() {
        Log.w(ConstUtil.TAG, TAG_PREFIX + "finishCurrentConnection - " + mConnectionState);
        if (mBluetoothGatt == null) {
            mConnectionState = BLEConnectionState.STATE_CONNECTING_CLOSE;
            mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_START_CONNECT_NEXT_DEVICE);
        } else if (mConnectionState != BLEConnectionState.STATE_DISCONNECTED) {
            mConnectionState = BLEConnectionState.STATE_CONNECTING_CLOSE;
            mBLEAdapterHandler.sendEmptyMessageDelayed(MSG_BLE_DISCONNECT_GATT_TIMEOUT, CONNECTION_TIMEOUT);
            Log.w(ConstUtil.TAG, TAG_PREFIX + "disconnect");
            mBluetoothGatt.disconnect();
        }
    }

    public boolean stop() {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "stop");
        scanDevices(false);
        //TODO: need to clear device list
        if (mTargetNodes.size() > 0) {
            Log.w(ConstUtil.TAG, TAG_PREFIX + "stop : but ble is running");
            finishCurrentConnection();
        }
        mBLEAdapterHandler.removeMessages(MSG_BLE_SCAN_TIMEOUT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_CONNECT_GATT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_GATT_CONNECTED);
        mBLEAdapterHandler.removeMessages(MSG_BLE_GATT_DISCONNECTED);
        mBLEAdapterHandler.removeMessages(MSG_BLE_SERVICE_DISCOVERED);
        mBLEAdapterHandler.removeMessages(MSG_BLE_START_CONNECT_NEXT_DEVICE);
        mBLEAdapterHandler.removeMessages(MSG_BLE_DISCOVER_SERVICES_TIMEOUT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_CONNECT_GATT_TIMEOUT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_DISCONNECT_GATT_TIMEOUT);
        return true;
    }

    public void release() {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "release");
        mInstance = null;
        mBLEAdapterHandler.removeMessages(MSG_BLE_SCAN_TIMEOUT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_CONNECT_GATT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_GATT_CONNECTED);
        mBLEAdapterHandler.removeMessages(MSG_BLE_GATT_DISCONNECTED);
        mBLEAdapterHandler.removeMessages(MSG_BLE_SERVICE_DISCOVERED);
        mBLEAdapterHandler.removeMessages(MSG_BLE_START_CONNECT_NEXT_DEVICE);
        mBLEAdapterHandler.removeMessages(MSG_BLE_DISCOVER_SERVICES_TIMEOUT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_CONNECT_GATT_TIMEOUT);
        mBLEAdapterHandler.removeMessages(MSG_BLE_DISCONNECT_GATT_TIMEOUT);
        mBLEAdapterHandler = null;
    }

    private void closeBluetoothGatt() {
        //assert: must called after gatt disconnected - mConnectionState = 0
        Log.w(ConstUtil.TAG, TAG_PREFIX + "closeBluetoothGatt : " + (mBluetoothGatt == null) + ", connect status : " + mConnectionState);
        mConnectionState = BLEConnectionState.STATE_DISCONNECTED;
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }


    private Handler mBLEAdapterHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_BLE_SCAN_TIMEOUT:
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_SCAN_TIMEOUT");
                    if (mScanning) {
                        scanDevices(false);
                    }
                    if (mTargetDevices.size() > 0) {
                        //Remove node which is not founded in TargetNodes
                        ArrayList<String> foundedIDs = new ArrayList<String>();
                        for (BluetoothDevice bleDevice : mTargetDevices) {
                            mTargetNodes.get(bleDevice.getName()).mMACAddress = bleDevice.getAddress();
                            foundedIDs.add(bleDevice.getName());
                        }
                        ArrayList<String> targetIDs = new ArrayList<String>();
                        targetIDs.addAll(mTargetNodes.keySet());
                        for (int i = 0; i < targetIDs.size(); i++) {
                            if (!foundedIDs.contains(targetIDs.get(i))) {
                                mTargetNodes.remove(targetIDs.get(i));
                            }
                        }

                        //Start to connect GATT
                        mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_CONNECT_GATT);
                    } else {
                        if (mBleManagerListener != null) {
                            mBleManagerListener.onFinished();
                        }
                    }
                    break;
                case MSG_BLE_CONNECT_GATT:
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_CONNECT_GATT - " + mConnectionState + ", Gatt = null - " + (mBluetoothGatt == null));
                    if (mTargetDevices.size() > 0) {
                        mConnectionState = BLEConnectionState.STATE_START_CONNECTING;
                        if (mBleManagerListener != null) {
                            mBleManagerListener.onStartToConnect(mTargetDevices.get(0).getName());
                        }
                        mBluetoothGatt = mTargetDevices.get(0).connectGatt(mContext, false, mBluetoothGattCallback);
                        mBLEAdapterHandler.sendEmptyMessageDelayed(MSG_BLE_CONNECT_GATT_TIMEOUT, CONNECTION_TIMEOUT);
                    } else {
                        Log.w(ConstUtil.TAG, TAG_PREFIX + "error target devices size = 0");
                        finishCurrentConnection();
                    }
                    break;
                case MSG_BLE_GATT_CONNECTED:
                    mConnectionState = BLEConnectionState.STATE_CONNECTED;
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "Connected to GATT server.");
                    // Attempts to discover services after successful connection.
                    if (mBluetoothGatt != null) {
                        Log.i(ConstUtil.TAG, TAG_PREFIX + "Attempting to start service discovery");
                        if (mBluetoothGatt.discoverServices()) {
                            mBLEAdapterHandler.sendEmptyMessageDelayed(MSG_BLE_DISCOVER_SERVICES_TIMEOUT, SCAN_PERIOD);
                        } else {
                            //TODO if it can't discover service???
                            Log.i(ConstUtil.TAG, TAG_PREFIX + "Attempting to start service discovery: failed");
                            finishCurrentConnection();
                        }
                    }
                    break;
                case MSG_BLE_GATT_DISCONNECTED:
                    mConnectionState = BLEConnectionState.STATE_DISCONNECTED;
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_GATT_DISCONNECTED");
                    //Close bluetooth gatt
                    closeBluetoothGatt();
                    //Start connect next device
                    mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_START_CONNECT_NEXT_DEVICE);
                    break;
                case MSG_BLE_START_CONNECT_NEXT_DEVICE:
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_START_CONNECT_NEXT_DEVICE : " + mConnectionState + ", gatt : " + (mBluetoothGatt == null) + ", retry " + msg.arg1);
                    //Assert mConnectionState should be STATE_DISCONNECTED && mBluetoothGatt = null
                    if (mTargetDevices.size() > 0) {
                        mTargetDevices.remove(0);
                    } else {
                        Log.v(ConstUtil.TAG, TAG_PREFIX + " error remove empty node");
                    }
                    //Start to connect next device with time delay
                    if (!mTargetDevices.isEmpty()) {
                        Log.i(ConstUtil.TAG, TAG_PREFIX + "start next device : " + mTargetDevices.get(0).getName());
                        mBLEAdapterHandler.sendEmptyMessageDelayed(MSG_BLE_CONNECT_GATT, NEXT_INTERVAL);
                    } else {
                        Log.i(ConstUtil.TAG, TAG_PREFIX + "ALL Finished");
                        if (mBleManagerListener != null) {
                            //Finish all device list operation
                            mBleManagerListener.onFinished();
                        }
                    }
                    break;
                case MSG_BLE_SERVICE_DISCOVERED:
                    break;

                case MSG_BLE_DISCOVER_SERVICES_TIMEOUT:
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_DISCOVER_SERVICES_TIMEOUT");
                    if (mBleManagerListener != null) {
                        mBleManagerListener.onFailed("Service 逾時");
                    }
                    finishCurrentConnection();
                    break;

                case MSG_BLE_CONNECT_GATT_TIMEOUT:
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_CONNECT_GATT_TIMEOUT");
                    if (mBleManagerListener != null) {
                        mBleManagerListener.onFailed("連線逾時");
                    }
                    finishCurrentConnection();
                    break;

                case MSG_BLE_DISCONNECT_GATT_TIMEOUT:
                    Log.i(ConstUtil.TAG, TAG_PREFIX + "MSG_BLE_DISCONNECT_GATT_TIMEOUT");
                    if (mBleManagerListener != null) {
                        mBleManagerListener.onFailed("斷線逾時 gatt");
                    }
                    closeBluetoothGatt();
                    mBLEAdapterHandler.sendEmptyMessage(MSG_BLE_START_CONNECT_NEXT_DEVICE);
                    break;
                default:
                    break;
            }

            super.handleMessage(msg);
        }
    };
}