package com.itri.toolkit.tag.bluetooth;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.util.Log;

import com.itri.toolkit.tag.object.SensorNode;
import com.itri.toolkit.tag.object.SensorNodeRecord;
import com.itri.toolkit.tag.utility.ConstUtil;
import com.itri.toolkit.tag.utility.NodeControllerUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Implement SensorTag Downlaod command
 */
public class NodeDownloadController extends NodeControllerBase {
    private static final String TAG_PREFIX = "[NodeDownloadNodeManager] ";
    private int checkCount = 0;
    private final byte OP_CODE = 0x03;
    private HashMap<String, ArrayList<SensorNodeRecord>> downloadResultMap = new HashMap<String, ArrayList<SensorNodeRecord>>();
    private ArrayList<SensorNodeRecord> downloadResult = new ArrayList<SensorNodeRecord>();
    private ArrayList<SensorNode> downloadNodeList = new ArrayList<SensorNode>();

    public NodeDownloadController(Context context) {
        super(context);
    }

    public void startDownloadNode(ArrayList<SensorNode> downloadNodes) {
        startDownloadNode(downloadNodes, false);
    }

    public void startDownloadNode(ArrayList<SensorNode> downloadNodes, boolean needScan) {
        downloadResultMap.clear();
        downloadResult.clear();
        downloadNodeList.clear();
        for (SensorNode node : downloadNodeList) {
            if (node.connectionStatus == SensorNode.ConnectionStatus.POLLING
                    || node.connectionStatus == SensorNode.ConnectionStatus.POLLING_WAITING) {
                this.downloadNodeList.add(node);
                node.connectionStatus = SensorNode.ConnectionStatus.DOWNLOADING;
            }
        }
        checkCount = downloadNodeList.size();
        setNodeControllerListener(nodeDownloadControllerListener);
        if (needScan) {
            setTargetNodeList(downloadNodes);
            startBLEOperation();
        } else {
            startConnectFoundedDevices(downloadNodes);
        }
        nodeStatusListener.onStart();
    }

    public HashMap<String, ArrayList<SensorNodeRecord>> getResult() {
        return downloadResultMap;
    }

    private NodeControllerListener nodeDownloadControllerListener = new NodeControllerListener() {
        @Override
        public void onAccessCharacteristic(BluetoothGatt gatt) {
            //set notification
            BluetoothGattService Service = gatt.getService(NodeControllerUtil.UUID_SERVICE);
            BluetoothGattCharacteristic characteristic = Service.getCharacteristic(NodeControllerUtil.UUID_CHARACTERISTIC_DATA);
            gatt.setCharacteristicNotification(characteristic, true);

            //trigger download
            byte[] data = new byte[11];
            data[0] = OP_CODE;
            for (int i = 1; i < data.length; i++) {
                data[i] = 0x00;
            }
            writeCharacteristic(NodeControllerUtil.UUID_SERVICE, NodeControllerUtil.UUID_CHARACTERISTIC_COMMAND, data);
        }

        @Override
        public void onNodeCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

        }

        @Override
        public void onNodeCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        }

        @Override
        public void onNodeCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.i(ConstUtil.TAG, TAG_PREFIX + "Callback:onCharacteristicChanged() download onNodeCharacteristicChanged");
            byte[] byte_data = characteristic.getValue();
            int[] int_data = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            for (int i = 0; i < byte_data.length - 1; i++) {
                int_data[i] = NodeControllerUtil.convertByteToInt(byte_data[i]);
            }
            int_data[11] = byte_data[11];

            String time = "20" + int_data[0]
                    + "-" + ((int_data[1] > 9) ? int_data[1] : ("0" + int_data[1]))
                    + "-" + ((int_data[2] > 9) ? int_data[2] : ("0" + int_data[2]))
                    + "T" + ((int_data[4] > 9) ? int_data[4] : ("0" + int_data[4]))
                    + ":" + ((int_data[5] > 9) ? int_data[5] : ("0" + int_data[5]))
                    + ":" + ((int_data[6] > 9) ? int_data[6] : ("0" + int_data[6]));

            String temperature = "";
            if (int_data[7] == 0) {
                temperature = "" + ((int_data[8] > 0) ? int_data[8] : "") + int_data[9] + "." + int_data[10];
            } else {
                temperature = "-" + int_data[8] + int_data[9] + "." + int_data[10];
            }

            String endFlag = String.valueOf(int_data[11]);

            Log.i(ConstUtil.TAG, TAG_PREFIX + "Callback:onCharacteristicChanged(), download data, time = " + time + " temperature = " + temperature + " endFlag = " + endFlag);
            downloadResult.add(new SensorNodeRecord(getCurrentTargetNodeID(), time, temperature));

            if (endFlag.compareTo("1") == 0) {
                ArrayList<SensorNodeRecord> sensorNodeRecords = new ArrayList<SensorNodeRecord>();
                sensorNodeRecords.addAll(downloadResult);
                downloadResultMap.put(getCurrentTargetNodeID(), sensorNodeRecords);
                finishCurrentConnection();
            }
        }

        @Override
        public void onAllNodeQueryFinished() {
            nodeStatusListener.onAllNodeFinished(checkCount == downloadResultMap.keySet().size());
        }

        @Override
        public void onFailed(String errorMsg) {
            nodeStatusListener.onNodeFailed(errorMsg);
        }

        @Override
        public void onStartToConnect(String nodeID) {
            downloadResult.clear();
            nodeStatusListener.onNodeStart(nodeID);
        }
    };

}
