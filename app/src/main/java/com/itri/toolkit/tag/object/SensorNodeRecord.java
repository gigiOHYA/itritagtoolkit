package com.itri.toolkit.tag.object;

public class SensorNodeRecord {
    public SensorNodeRecord(String nodeID, String timestamp, String temperature, String power) {
        this.timestamp = timestamp;
        this.temperature = temperature;
        this.power = power;
        this.nodeID = nodeID;
    }

    public SensorNodeRecord(String nodeID, String timestamp, String temperature) {
        this.timestamp = timestamp;
        this.temperature = temperature;
        this.power = "100";
        this.nodeID = nodeID;
    }

    public String timestamp;
    public String temperature;
    public String power;
    public String nodeID;
}
