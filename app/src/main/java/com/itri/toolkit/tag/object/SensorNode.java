package com.itri.toolkit.tag.object;

import java.util.ArrayList;

public class SensorNode {
    public String ID = "";
    public int connectionStatus = ConnectionStatus.NONE;
    public SensorNodeRecord currentData = new SensorNodeRecord("", "", "", "");
    public String mMACAddress = "";
    public String pollingState = "";//Failed message or polling timestamp

    public static class ConnectionStatus {
        public final static int NONE = 0;
        public final static int INITIALIZING = 1;
        public final static int INITIALIZED = 2;
        public final static int POLLING = 3;
        public final static int POLLING_WAITING = 4;
        public final static int DOWNLOADING = 5;
    }

    public SensorNode(String id) {
        ID = id;
        resetCurrentData();
    }

    private ArrayList<SensorNodeRecord> nodeRecords = new ArrayList<SensorNodeRecord>();


    public void addNodeRecord(SensorNodeRecord record) {
        nodeRecords.add(record);
    }

    public void resetNodeRecords() {
        nodeRecords.clear();
    }

    public ArrayList<SensorNodeRecord> getNodeRecords() {
        return nodeRecords;
    }

    public String getConnectionText() {
        switch (connectionStatus) {
            case ConnectionStatus.NONE:
                return "未連結";
            case ConnectionStatus.INITIALIZING:
                return "初始化連結中...";
            case ConnectionStatus.INITIALIZED:
                return "初始化完成";
            case ConnectionStatus.POLLING:
                return "輪詢中";
            case ConnectionStatus.POLLING_WAITING:
                return "等待輪詢";
            case ConnectionStatus.DOWNLOADING:
                return "下載中";
            default:
                return "未連結";
        }
    }
//
//    public int getBoxRes() {
//        switch (connectionStatus) {
//            case ConnectionStatus.NONE:
//                return R.drawable.icon_box;
//            case ConnectionStatus.INITIALIZING:
//                return R.drawable.icon_box;
//            case ConnectionStatus.INITIALIZED:
//                return R.drawable.icon_box_initialized;
//            case ConnectionStatus.POLLING:
//                return R.drawable.icon_box_polling;
//            case ConnectionStatus.POLLING_WAITING:
//                if (pollingState.startsWith("[")) {
//                    return R.drawable.icon_box_polling_waiting_failed;
//                } else {
//                    return R.drawable.icon_box_polling_waiting;
//                }
//            case ConnectionStatus.DOWNLOADING:
//                return R.drawable.icon_box_polling;
//            default:
//                return R.drawable.icon_box;
//        }
//    }

    public void resetCurrentData() {
        currentData.power = "--";
        currentData.temperature = "--";
        currentData.timestamp = "";
    }
}
