package com.itri.toolkit.tag.bluetooth;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.util.Log;

import com.itri.toolkit.tag.object.SensorNode;
import com.itri.toolkit.tag.utility.ConstUtil;
import com.itri.toolkit.tag.utility.NodeControllerUtil;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Implement SensorTag Initial command
 */
public class NodeInitialController extends NodeControllerBase {
    private static final String TAG_PREFIX = "[NodeInitialController] ";
    private final byte OP_CODE = 0x01;
    private int checkCount = 0;
    private byte[] package_data = new byte[11];
    private ArrayList<String> initializedIDs = new ArrayList<String>();

    public void startInitialNodes(ArrayList<SensorNode> targetNodes) {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "startInitialNodes - " + targetNodes.toString());
        initializedIDs.clear();
        checkCount = targetNodes.size();
        setNodeControllerListener(nodeInitialControllerListener);
        setTargetNodeList(targetNodes);
        startBLEOperation();
        nodeStatusListener.onStart();
    }

    public NodeInitialController(Context context) {
        super(context);
    }

    public ArrayList<String> getInitializedIDs() {
        return initializedIDs;
    }

    private NodeControllerListener nodeInitialControllerListener = new NodeControllerListener() {
        @Override
        public void onAccessCharacteristic(BluetoothGatt gatt) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onAccessCharacteristic");
            //set initial value into node
            Calendar c = Calendar.getInstance();
            package_data[0] = OP_CODE;
            package_data[1] = NodeControllerUtil.convertIntToByte(c.get(Calendar.YEAR) % 1000); //we only need last two numbers of year(2014 -> 14)
            package_data[2] = NodeControllerUtil.convertIntToByte(c.get(Calendar.MONTH) + 1);//month 0 is January
            package_data[3] = NodeControllerUtil.convertIntToByte(c.get(Calendar.DATE));
            package_data[4] = NodeControllerUtil.convertIntToByte(c.get(Calendar.DAY_OF_WEEK) - 1); //day 0 is sunday
            package_data[5] = NodeControllerUtil.convertIntToByte(c.get(Calendar.HOUR_OF_DAY));
            package_data[6] = NodeControllerUtil.convertIntToByte(c.get(Calendar.MINUTE));
            package_data[7] = NodeControllerUtil.convertIntToByte(c.get(Calendar.SECOND));
            package_data[8] = NodeControllerUtil.convertIntToByte(NodeControllerUtil.INTERVAL_HOUR);
            package_data[9] = NodeControllerUtil.convertIntToByte(NodeControllerUtil.INTERVAL_MINUTE);
            package_data[10] = NodeControllerUtil.convertIntToByte(NodeControllerUtil.INTERVAL_SECOND);
            writeCharacteristic(NodeControllerUtil.UUID_SERVICE, NodeControllerUtil.UUID_CHARACTERISTIC_COMMAND, package_data);
        }

        @Override
        public void onNodeCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onNodeCharacteristicWrite Initial");
            initializedIDs.add(getCurrentTargetNodeID());
            finishCurrentConnection();
        }

        @Override
        public void onNodeCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

        }

        @Override
        public void onNodeCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        }

        @Override
        public void onAllNodeQueryFinished() {
            nodeStatusListener.onAllNodeFinished(checkCount == initializedIDs.size());
        }

        @Override
        public void onFailed(String errorMsg) {
            nodeStatusListener.onNodeFailed(errorMsg);
        }

        @Override
        public void onStartToConnect(String nodeID) {
            nodeStatusListener.onNodeStart(nodeID);
        }
    };
}
