package com.itri.toolkit.tag.bluetooth;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.util.Log;

import com.itri.toolkit.tag.object.SensorNode;
import com.itri.toolkit.tag.utility.ConstUtil;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Abstract class for Sensor Tag command
 */
public abstract class NodeControllerBase {
    private static final String TAG_PREFIX = "[NodeControllerBase] ";
    private static NodeControllerListener mNodeControllerListener;
    private static ArrayList<SensorNode> targetNodeIDList = new ArrayList<SensorNode>();
    private boolean isConnecting = false;
    private BLEController mBLEController;
    protected Context mContext;
    protected NodeStatusListener nodeStatusListener;

    //For listen node status
    public interface NodeStatusListener {
        void onAllNodeFinished(boolean allSuccess);

        void onNodeFailed(String msg);

        void onStart();

        void onNodeStart(String node);
    }

    protected abstract class NodeControllerListener {
        public abstract void onAccessCharacteristic(BluetoothGatt gatt);

        public abstract void onNodeCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);

        public abstract void onNodeCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);

        public abstract void onNodeCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic);

        public abstract void onAllNodeQueryFinished();

        public abstract void onFailed(String errorMsg);

        public abstract void onStartToConnect(String nodeID);
    }

    public NodeControllerBase(Context context) {
        this.mContext = context;
        initComponent();
    }
    public String getCurrentTargetNodeID() {
        return mBLEController.getTargetID();
    }

    public void setNodeStatusListener(NodeStatusListener listener) {
        nodeStatusListener = listener;
    }

    //Stop BLE operation
    public boolean stop() {
        //TODO:Need close?? or need finish current job and start next one?
        return mBLEController.stop();
    }

    protected void setNodeControllerListener(NodeControllerListener listener) {
        mNodeControllerListener = listener;
    }

    protected void setTargetNodeList(ArrayList<SensorNode> targetNodeIDList) {
        this.targetNodeIDList.clear();
        this.targetNodeIDList.addAll(targetNodeIDList);
        Log.v(ConstUtil.TAG, TAG_PREFIX + "setTargetNodeList : " + targetNodeIDList.toString());
    }

    protected void startBLEOperation() {
        Log.v(ConstUtil.TAG, TAG_PREFIX + "startBLEOperation : " + targetNodeIDList.toString());
        mBLEController.enableBLE();
        mBLEController.startOperation(targetNodeIDList);
    }

    protected void writeCharacteristic(UUID service, UUID characteristic, byte[] data) {
        mBLEController.writeCharacteristic(service, characteristic, data);
    }

    protected void readCharacteristic(UUID service, UUID characteristic) {
        mBLEController.readCharacteristic(service, characteristic);
    }

    protected void finishCurrentConnection() {
        mBLEController.finishCurrentConnection();
    }

    protected void startConnectFoundedDevices() {
        mBLEController.startConnectFoundedDevices();
    }

    protected void startConnectFoundedDevices(ArrayList<SensorNode> nodes) {
        mBLEController.startConnectRemoteDevices(nodes);
    }
    private void initComponent() {
        mBLEController = BLEController.getInstance(mContext);
        mBLEController.setServiceListener(nodeStateListener);
    }

    private BLEController.BLEManagerListener nodeStateListener = new BLEController.BLEManagerListener() {
        @Override
        public void onConnected() {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onConnected");
            isConnecting = true;
        }

        @Override
        public void onFinished() {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onFinished : " + isConnecting);
            mNodeControllerListener.onAllNodeQueryFinished();
            isConnecting = false;
        }

        @Override
        public void onServiceDiscovered(BluetoothGatt bluetoothGatt, int status) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onServiceDiscovered : ");
            if (mNodeControllerListener != null) {
                mNodeControllerListener.onAccessCharacteristic(bluetoothGatt);
            }
        }

        //TODO: release bluetooth gatt?
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicWrite");
            if (mNodeControllerListener != null) {
                mNodeControllerListener.onNodeCharacteristicWrite(gatt, characteristic, status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicRead");
            if (mNodeControllerListener != null) {
                mNodeControllerListener.onNodeCharacteristicRead(gatt, characteristic, status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onCharacteristicChanged");
            if (mNodeControllerListener != null) {
                mNodeControllerListener.onNodeCharacteristicChanged(gatt, characteristic);
            }
        }

        @Override
        public void onFailed(String errorMsg) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onFailed : " + errorMsg + ", isConnecting : " + isConnecting);
            if (mNodeControllerListener != null) {
                mNodeControllerListener.onFailed(errorMsg);
            }
        }

        @Override
        public void onStartToConnect(String nodeID) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onStartToConnect : " + nodeID);
            if (mNodeControllerListener != null) {
                mNodeControllerListener.onStartToConnect(nodeID);
            }
        }
    };
}
