package com.itri.toolkit.tag.utility;

import java.util.UUID;

public class NodeControllerUtil {

    //convert time to the special type in node
    public static byte convertIntToByte(int value) {
        byte high = (byte) (Byte.parseByte("" + value / 10) & 0xFF);
        byte low = (byte) (Byte.parseByte("" + value % 10) & 0xFF);
        byte result = (byte) (high << 4 | low);
        return result;
    }

    //convert special type byte to int
    public static int convertByteToInt(byte value) {
        int result;
        int high = (int) ((value >> 4) & 0x0F);
        int low = (int) (value & 0x0F);
        result = high * 10 + low;
        return result;
    }

    //Firmware service
    public static final UUID UUID_SERVICE = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_CHARACTERISTIC_COMMAND = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_CHARACTERISTIC_DATA = UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb");

    //initial value
    public static int INTERVAL_HOUR = 0;   //the sensing interval - hour
    public static int INTERVAL_MINUTE = 0;   //the sensing interval - minute
    public static int INTERVAL_SECOND = 10;  //the sensing interval - second, the default value is 10s
    public static int INTERVAL_APP_POLLING = 15;  //the query interval time,  the default value is 15s;

    public static boolean connectServer = true;
}
